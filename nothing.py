import requests
class bcolors:
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

GITLAB_URL = "https://gitlab.com"
PRIVATE_ACCESS_TOKEN = ""
GROUP_ID = 9677478 

def gl_list(type,group_id):
    url = f"{GITLAB_URL}/api/v4/groups/{group_id}/{type}/"
    headers = {"Private-Token": PRIVATE_ACCESS_TOKEN}
    response = requests.get(url, headers=headers)
    return response.json()

def make_private(_type,id):
        
        if _type=="project":
            url = f"{GITLAB_URL}/api/v4/projects/{id}"
        elif _type=="subgroup":
            url = f"{GITLAB_URL}/api/v4/groups/{id}"
        else:
            print("Entschuldigung, hier gibt es keine error handling, we die like heroes")
        headers = {"Private-Token": PRIVATE_ACCESS_TOKEN}
        data = {"visibility": "private"}
        response = requests.put(url, headers=headers, data=data)
        if response.status_code == 200:
            print(f"made private.")
        else:
            print(f"scheiße", response.json())

if __name__ == "__main__":
    for s in gl_list("subgroups",GROUP_ID):
        print(f"\n[{bcolors.OKCYAN}{s['id']}{bcolors.ENDC}] {bcolors.BOLD}{s['name']} {bcolors.ENDC}")
        for p in gl_list("projects",s['id']):
            print(f"          [{bcolors.OKGREEN}{p['id']}{bcolors.ENDC}] {p['name']}",end=" :: ")
            make_private("project",p['id'])
        print(f"[{bcolors.OKCYAN}{s['id']}{bcolors.ENDC}]", end=" ")
        make_private("subgroup",s['id'])